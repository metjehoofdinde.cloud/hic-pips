variable "servicelist" {
  description = "list of services to configure pips for"
  type        = "list"

  default = [
    "moodle",
  ]
}

variable "resource_prefix" {
  type    = "string"
  default = "hic"
}

variable "location" {
  type    = "string"
  default = "west europe"
}

variable "tags" {
  type = "map"

  default = {
    terrafrommanaged = "true"
  }
}
