output "service_fqdn" {
  value = "${zipmap(var.servicelist, azurerm_public_ip.serviceip.*.fqdn)}"
}

output "service_pip_id" {
  value = "${zipmap(var.servicelist, azurerm_public_ip.serviceip.*.id)}"
}
