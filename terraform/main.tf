resource "azurerm_resource_group" "pip" {
  name     = "${var.resource_prefix}-pip"
  location = "${var.location}"
}

resource "azurerm_public_ip" "serviceip" {
  resource_group_name = "${azurerm_resource_group.pip.name}"
  location            = "${azurerm_resource_group.pip.location}"

  name  = "${var.resource_prefix}-${element(var.servicelist, count.index)}-pip"
  count = "${length(var.servicelist)}"

  domain_name_label            = "${var.resource_prefix}${element(var.servicelist, count.index)}"
  public_ip_address_allocation = "dynamic"

  tags = "${var.tags}"
}
