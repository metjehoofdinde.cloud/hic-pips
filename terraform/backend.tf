terraform {
  backend "azurerm" {
    resource_group_name  = "hic-tfstate"
    storage_account_name = "hictfstate"
    container_name       = "terraform-state"
    key                  = "prod.pip.tfstate"
  }
}
